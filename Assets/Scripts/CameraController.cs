﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public static CameraController Instance;

    private Animator m_cameraAnimator;


    private void Awake()
    {
        Instance = this;

        m_cameraAnimator = gameObject.GetComponent<Animator>();
    }
    
    public void SetAnimation(string inString)
    {
        m_cameraAnimator.SetTrigger(inString);
    }

}
