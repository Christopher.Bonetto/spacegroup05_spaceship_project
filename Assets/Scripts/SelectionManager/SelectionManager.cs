﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SelectionManager : MonoBehaviour
{
    public static SelectionManager Instance;

    [Space, Header("COLLECTION TO READ")]
    public AllCollectionList MyCollection;

    #region Wings

    private List<GameObject> m_wingsList = new List<GameObject>();

    [SerializeField, Header("Wings Container")] private Transform m_wingsContainer;
    public Transform WingsContainer { get { return m_wingsContainer; } }

    private int m_wingsIndex = -1;
    public int WingsIndex
    {
        get
        {
            return m_wingsIndex;
        }
        set
        {
            m_wingsIndex = value;

            if (m_wingsIndex > (MyCollection.ListsOfEquipments[3].ListOfElement.Count - 1))
            {
                m_wingsIndex = 0;
            }
            else if (m_wingsIndex < 0)
            {
                m_wingsIndex = MyCollection.ListsOfEquipments[3].ListOfElement.Count - 1;
            }

            EventManager.TriggerEvent<Equipment,GameObject>(EventID.OnChangeEquipmentPreview, MyCollection.ListsOfEquipments[3].ListOfElement[m_wingsIndex], m_wingsList[m_wingsIndex]);
        }
    }

    #endregion

    #region Engine

    private List<GameObject> m_engineList = new List<GameObject>();

    [SerializeField, Header("Engine Container")] private Transform m_engineContainer;
    public Transform EngineContainer { get { return m_engineContainer; } }

    private int m_engineIndex = -1;
    public int EngineIndex
    {
        get
        {
            return m_engineIndex;
        }
        set
        {
            m_engineIndex = value;

            if (m_engineIndex > (MyCollection.ListsOfEquipments[1].ListOfElement.Count - 1))
            {
                m_engineIndex = 0;
            }
            else if (m_engineIndex < 0)
            {
                m_engineIndex = MyCollection.ListsOfEquipments[1].ListOfElement.Count - 1;
            }

            EventManager.TriggerEvent<Equipment, GameObject>(EventID.OnChangeEquipmentPreview, MyCollection.ListsOfEquipments[1].ListOfElement[m_engineIndex], m_engineList[m_engineIndex]);
        }
    }

    #endregion

    #region Thrusters

    private List<GameObject> m_thrustersList = new List<GameObject>();

    [SerializeField, Header("Thrusters Container")] private Transform m_thrustersContainer;
    public Transform ThrustersContainer { get { return m_thrustersContainer; }}

    private int m_thrustersIndex = -1;
    public int ThrustersIndex
    {
        get
        {
            return m_thrustersIndex;
        }
        set
        {
            m_thrustersIndex = value;
            if (m_thrustersIndex > (MyCollection.ListsOfEquipments[2].ListOfElement.Count -1))
            {
                m_thrustersIndex = 0;
            }
            else if (m_thrustersIndex < 0)
            {
                m_thrustersIndex = MyCollection.ListsOfEquipments[2].ListOfElement.Count - 1;
            }

            EventManager.TriggerEvent<Equipment, GameObject>(EventID.OnChangeEquipmentPreview, MyCollection.ListsOfEquipments[2].ListOfElement[m_thrustersIndex], m_thrustersList[m_thrustersIndex]);
        }
    }

    #endregion

    #region Body

    private List<GameObject> m_bodyList = new List<GameObject>();

    [SerializeField, Header("Body Container")] private Transform m_bodyContainer;
    public Transform BodyContainer { get { return m_bodyContainer; } }

    private int m_bodyIndex = -1;
    public int BodyIndex
    {
        get
        {
            return m_bodyIndex;
        }
        set
        {
            m_bodyIndex = value;

            if(m_bodyIndex > (MyCollection.ListsOfEquipments[0].ListOfElement.Count - 1))
            {
                m_bodyIndex = 0;
            }
            else if(m_bodyIndex < 0)
            {
                m_bodyIndex = MyCollection.ListsOfEquipments[0].ListOfElement.Count - 1;
            }

            EventManager.TriggerEvent<Equipment, GameObject>(EventID.OnChangeEquipmentPreview, MyCollection.ListsOfEquipments[0].ListOfElement[m_bodyIndex], m_bodyList[m_bodyIndex]);
        }
    }

    #endregion

    #region Characters

    public List<Character> m_charactersList { get; private set; }

    private int m_charactersIndex = -1;
    public int CharactersIndex
    {
        get
        {
            return m_charactersIndex;
        }
        set
        {
            m_charactersIndex = value;

            EventManager.TriggerEvent<Character>(EventID.OnChangeCharacterPreview, MyCollection.ListOfAllCharacter.Characters[value]);
        }
    }

    private Character m_selectedCharacter = null;
    public Character SelectedCharacter
    {
        get
        {
            return m_selectedCharacter;
        }
        set
        {
            m_selectedCharacter = value;

            EventManager.TriggerEvent<Character>(EventID.OnCharacterSelected, value);
        }
    }

    #endregion

    #region Behaviour Cicle

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        TakeEquipmentsToCollection();

        TakeCharactersToCollection();

        StartShipEquipmentSet();
    }

    #endregion

    #region Starting initialization

    public void TakeEquipmentsToCollection()
    {
        for (int i = 0; i < MyCollection.ListsOfEquipments.Count; i++)
        {
            for(int j = 0; j < MyCollection.ListsOfEquipments[i].ListOfElement.Count; j++)
            {
                InstantiateNewEquipment(MyCollection.ListsOfEquipments[i].ListOfElement[j]);

                EventManager.TriggerEvent<Equipment, int>(EventID.OnNewEquipmentCreated, MyCollection.ListsOfEquipments[i].ListOfElement[j], j);
            }
        }
    }

    public void TakeCharactersToCollection()
    {
        m_charactersList = new List<Character>();

        for (int i = 0; i < MyCollection.ListOfAllCharacter.Characters.Count; i++)
        {
            m_charactersList.Add(MyCollection.ListOfAllCharacter.Characters[i]);

            EventManager.TriggerEvent<Character, int>(EventID.OnNewCharacterCreated, m_charactersList[i], i);
        }
    }

    public void InstantiateNewEquipment(Equipment inEquipment)
    {
        EquipType type = inEquipment.CurrentEquipType;
        GameObject equipment = Instantiate(inEquipment.m_equipPrefab) as GameObject;

        switch (type)
        {
            case EquipType.Body:
                AddToList(m_bodyList, equipment);
                equipment.transform.SetParent(m_bodyContainer.transform);
                break;

            case EquipType.Engine:
                AddToList(m_engineList, equipment);
                equipment.transform.SetParent(m_engineContainer.transform);
                break;

            case EquipType.Thrusters:
                AddToList(m_thrustersList, equipment);
                equipment.transform.SetParent(m_thrustersContainer.transform);
                break;

            case EquipType.Wings:
                AddToList(m_wingsList, equipment);
                equipment.transform.SetParent(m_wingsContainer.transform);
                break;
        }
        equipment.transform.localPosition = new Vector3(0, 0, 0);
        equipment.SetActive(false);
    }

    public void AddToList(List<GameObject> inList, GameObject inEquipment)
    {
        if (!inList.Contains(inEquipment))
        {
            inList.Add(inEquipment);
        }
    }

    public void StartShipEquipmentSet()
    {
        BodyIndex++;
        WingsIndex++;
        ThrustersIndex++;
        EngineIndex++;
    }

    #endregion

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ReloadScene();
        }
    }

    public void ReloadScene()
    {
        SceneManager.LoadScene(0);
    }
}
