﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EquipType
{
    Body,
    Wings,
    Engine,
    Thrusters,
}

[System.Serializable]
public struct Stats
{
    public int Speed;
    public int Acceleration;
    public int Manovrability;
    public int Mass;
}

[CreateAssetMenu(menuName = "Parts/New Equippable Item", fileName = "Equipment")]
public class Equipment : ScriptableObject
{
    [SerializeField] private EquipType m_currentEquipType;
    public EquipType CurrentEquipType
    {
        get
        {
            return m_currentEquipType;
        }
        set
        {
            m_currentEquipType = value;
        }
    }

    [Space, Space]
    public GameObject m_equipPrefab;

    [Space,Space]
    public Sprite m_equipmentSprite; 
    
    [Space,Space]
    public Stats m_equipmentStats;
}
