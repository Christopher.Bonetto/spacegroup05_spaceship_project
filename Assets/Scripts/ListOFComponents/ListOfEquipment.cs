﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Parts/New List Equipment", fileName = "EquipList")]
public class ListOfEquipment : ScriptableObject
{
    public List<Equipment> ListOfElement = new List<Equipment>();
}
