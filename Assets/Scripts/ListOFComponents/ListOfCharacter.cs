﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Parts/New List Character", fileName = "character list")]
public class ListOfCharacter : ScriptableObject
{
    public List<Character> Characters = new List<Character>();
}
