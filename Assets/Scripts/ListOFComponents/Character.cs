﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TypeOfValue
{
    Speed,
    Acceleration,
    Manovrability,
    Mass
}

[System.Serializable]
public struct CharacterStats
{
    public TypeOfValue Type;
    public int Value;
    public Sprite ImageStat;
}

[CreateAssetMenu(menuName = "Parts/New Character", fileName = "Character")]
public class Character : ScriptableObject
{
    [Space, Header("BUFF AND DEBUFF")]
    public CharacterStats BuffStat;

    [Space,Space]
    public CharacterStats DebuffStat;


    [Space, Space, Header("Other informations")]
    public Sprite CharacterSprite;
    public Sprite CharacterSelectedSprite;

    [Space, Space]
    [TextArea] public string Description; 

}
