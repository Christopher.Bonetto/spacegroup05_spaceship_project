﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Parts/New Collection Contenitor", fileName = "Contenitor")]
public class AllCollectionList : ScriptableObject
{
    [Space, Header("EQUIPMENTS LISTS")]
    public List<ListOfEquipment> ListsOfEquipments = new List<ListOfEquipment>();

    

    [Space, Header("CHARACTERS LIST"), Space,Space]
    public ListOfCharacter ListOfAllCharacter;

}
