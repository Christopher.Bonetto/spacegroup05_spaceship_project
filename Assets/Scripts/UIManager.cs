﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance;

    [SerializeField] private GameObject m_selectionCharPanel;
    [SerializeField] private GameObject m_selectionEquipPanel;

    [Header("Swap image alpha effect")]
    [SerializeField] private float m_minAlphaValue = 0f;
    [SerializeField] private float m_timeToChangeAlpha = 0f;


    #region Engine UI

    [SerializeField, Header("Engine")] private Image[] m_engineImages = new Image[3];

    private Image m_currentEngineImage;

    private int m_engineIndexUI;
    public int EngineIndexUI
    {
        get
        {
            return m_engineIndexUI;
        }
        private set
        {
            m_engineIndexUI = value;

            if(m_currentEngineImage != null)
            {
                m_currentEngineImage.CrossFadeAlpha(m_minAlphaValue, m_timeToChangeAlpha, false);
            }

            m_currentEngineImage = m_engineImages[value];

            m_currentEngineImage.CrossFadeAlpha(1, m_timeToChangeAlpha, false);
        }
    }

    #endregion

    #region Thrusters UI

    [SerializeField,Header("Thrusters")] private Image[] m_thrustersImages = new Image[3];

    private Image m_currentThrustersImage;

    private int m_thrustersIndexUI;
    public int ThrustersIndexUI
    {
        get
        {
            return m_thrustersIndexUI;
        }
        private set
        {
            m_thrustersIndexUI = value;

            if (m_currentThrustersImage != null)
            {
                m_currentThrustersImage.CrossFadeAlpha(m_minAlphaValue, m_timeToChangeAlpha, false);
            }

            m_currentThrustersImage = m_thrustersImages[value];

            m_currentThrustersImage.CrossFadeAlpha(1, m_timeToChangeAlpha, false);
        }
    }

    #endregion

    #region Wings UI

    [SerializeField,Header("Wings")] private Image[] m_wingsImages = new Image[3];

    private Image m_currentWingsImage;

    private int m_wingsIndexUI;
    public int WingsIndexUI
    {
        get
        {
            return m_wingsIndexUI;
        }
        private set
        {
            m_wingsIndexUI = value;

            if (m_currentWingsImage != null)
            {
                m_currentWingsImage.CrossFadeAlpha(m_minAlphaValue, m_timeToChangeAlpha, false);
            }

            m_currentWingsImage = m_wingsImages[value];

            m_currentWingsImage.CrossFadeAlpha(1, m_timeToChangeAlpha, false);
        }
    }

    #endregion

    #region Body UI

    [SerializeField, Header("Body")] private Image[] m_bodyImages = new Image[3];

    private Image m_currentBodyImage;

    private int m_bodyIndexUI;
    public int BodyIndexUI
    {
        get
        {
            return m_bodyIndexUI;
        }
        private set
        {
            m_bodyIndexUI = value;

            if (m_currentBodyImage != null)
            {
                m_currentBodyImage.CrossFadeAlpha(m_minAlphaValue, m_timeToChangeAlpha, false);
            }

            m_currentBodyImage = m_bodyImages[value];

            m_currentBodyImage.CrossFadeAlpha(1, m_timeToChangeAlpha, false);
        }
    }

    #endregion

    #region Characters UI

    [Header("Characters")]

    [SerializeField] private GameObject m_descriptionPanel;
    [SerializeField] private Image m_selectedCharImage;

    [Space]

    [SerializeField] private GameObject m_charactersHolder;
    [SerializeField] private GameObject m_characterButton;

    [Space]

    [SerializeField] private Image m_selectedCharPreviewImage;
    [SerializeField] private Text m_descriptionArea;
    [SerializeField] private Text m_nameArea;

    [Space]

    [SerializeField] private Image m_buffImage;
    [SerializeField] private Image m_debuffImage;

    #endregion

    #region Stats slider

    [Header("Sliders Reference")]

    [SerializeField] private Slider m_speedStatSlider;
    [SerializeField] private Slider m_accelerationStatSlider;
    [SerializeField] private Slider m_manovrabilityStatSlider;
    [SerializeField] private Slider m_massStatSlider;

    #endregion

    #region Events

    private void OnEnable()
    {
        EventManager.SubscribeTo<Equipment, GameObject>(EventID.OnChangeEquipmentPreview, ActivateEquipmentImage);

        EventManager.SubscribeTo<Equipment, int>(EventID.OnNewEquipmentCreated, FillEquipmentsList);

        EventManager.SubscribeTo<Stats>(EventID.OnChangeStats, ChangeStatsVisual);

        EventManager.SubscribeTo<Character, int>(EventID.OnNewCharacterCreated, FillCharactersList);

        EventManager.SubscribeTo<Character>(EventID.OnChangeCharacterPreview, ChangeCharacterPreview);

        EventManager.SubscribeTo<Character>(EventID.OnCharacterSelected, SelectedCharacterUI);
    }
    private void OnDisable()
    {
        EventManager.UnsubscribeFrom<Equipment, GameObject>(EventID.OnChangeEquipmentPreview, ActivateEquipmentImage);

        EventManager.UnsubscribeFrom<Equipment, int>(EventID.OnNewEquipmentCreated, FillEquipmentsList);

        EventManager.UnsubscribeFrom<Stats>(EventID.OnChangeStats, ChangeStatsVisual);

        EventManager.UnsubscribeFrom<Character, int>(EventID.OnNewCharacterCreated, FillCharactersList);

        EventManager.UnsubscribeFrom<Character>(EventID.OnChangeCharacterPreview, ChangeCharacterPreview);

        EventManager.UnsubscribeFrom<Character>(EventID.OnCharacterSelected, SelectedCharacterUI);
    }

    #endregion

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        SwapEnablePanel();

        m_descriptionPanel.SetActive(false);
    }


    #region Fill Equip Images with sprite

    public void FillEquipmentsList(Equipment inNewEquip, int index)
    {
        EquipType type = inNewEquip.CurrentEquipType;

        switch (type)
        {
            case EquipType.Body:
                m_bodyImages[index].sprite = inNewEquip.m_equipmentSprite;
                m_bodyImages[index].preserveAspect = true;
                m_bodyImages[index].CrossFadeAlpha(m_minAlphaValue, 0f, false);
                break;

            case EquipType.Engine:
                m_engineImages[index].sprite = inNewEquip.m_equipmentSprite;
                m_engineImages[index].preserveAspect = true;
                m_engineImages[index].CrossFadeAlpha(m_minAlphaValue, 0f, false);
                break;

            case EquipType.Thrusters:
                m_thrustersImages[index].sprite = inNewEquip.m_equipmentSprite;
                m_thrustersImages[index].preserveAspect = true;
                m_thrustersImages[index].CrossFadeAlpha(m_minAlphaValue, 0f, false);
                break;

            case EquipType.Wings:
                m_wingsImages[index].sprite = inNewEquip.m_equipmentSprite;
                m_wingsImages[index].preserveAspect = true;
                m_wingsImages[index].CrossFadeAlpha(m_minAlphaValue, 0f, false);
                break;
        }
    }

    #endregion

    #region Fill Characters Button and Images

    public void FillCharactersList(Character inChar, int inIndex)
    {
        GameObject button = Instantiate(m_characterButton, m_charactersHolder.transform) as GameObject;

        ChangeCharButton ccRef = button.GetComponent<ChangeCharButton>();

        if(ccRef != null)
        {
            ccRef.CharIndex = inIndex;
            ccRef.SetNewSprite(inChar.CharacterSprite);
        }
    }

    #endregion

    #region Activate equipment UI

    public void ActivateEquipmentImage(Equipment inNewEquip, GameObject inObj)
    {
        EquipType type = inNewEquip.CurrentEquipType;

        switch (type)
        {
            case EquipType.Body:
                BodyIndexUI = SelectionManager.Instance.BodyIndex;
                break;

            case EquipType.Engine:
                EngineIndexUI = SelectionManager.Instance.EngineIndex;
                break;

            case EquipType.Thrusters:
                ThrustersIndexUI = SelectionManager.Instance.ThrustersIndex;
                break;

            case EquipType.Wings:
                WingsIndexUI = SelectionManager.Instance.WingsIndex;
                break;
        }
    }

    #endregion

    #region Change Character UI

    public void ChangeCharacterPreview(Character inChar)
    {
        m_descriptionPanel.SetActive(true);

        string description;

        description = inChar.Description;

        m_descriptionArea.text = description;
        m_nameArea.text = inChar.name;

        m_selectedCharPreviewImage.sprite = inChar.CharacterSelectedSprite;
        m_selectedCharPreviewImage.preserveAspect = true;

        if(inChar.BuffStat.Value > 0)
        {
            m_buffImage.gameObject.SetActive(true);
            m_buffImage.sprite = inChar.BuffStat.ImageStat;
            m_buffImage.preserveAspect = true;
        }
        else
        {
            m_buffImage.gameObject.SetActive(false);
        }
        
        if(inChar.DebuffStat.Value > 0)
        {
            m_debuffImage.gameObject.SetActive(true);
            m_debuffImage.sprite = inChar.DebuffStat.ImageStat;
            m_debuffImage.preserveAspect = true;
        }
        else
        {
            m_debuffImage.gameObject.SetActive(false);
        }
        
    }

    #endregion

    #region Change sliders values

    public void ChangeStatsVisual(Stats inStats)
    {
        float sliderSpeed = (float)inStats.Speed / 12;
        float sliderAcceleration = (float)inStats.Acceleration / 12;
        float sliderManovrability = (float)inStats.Manovrability / 12;
        float sliderMass = (float)inStats.Mass / 12;

        m_speedStatSlider.value = sliderSpeed;
        m_accelerationStatSlider.value = sliderAcceleration;
        m_manovrabilityStatSlider.value = sliderManovrability;
        m_massStatSlider.value = sliderMass;
    }

    #endregion


    public void SelectedCharacterUI(Character inChar)
    {
        if(inChar != null)
        {
            SwapEnablePanel(false);
            m_selectedCharImage.sprite = inChar.CharacterSelectedSprite;
        }
        else
        {
            SwapEnablePanel();
        }
    }

    public void SwapEnablePanel(bool isCharPanelOpen = true)
    {
        if (isCharPanelOpen)
        {
            m_selectionCharPanel.SetActive(true);
            m_selectionEquipPanel.SetActive(false);
        }
        else
        {
            m_selectionCharPanel.SetActive(false);
            m_selectionEquipPanel.SetActive(true);
        }
    }
}
