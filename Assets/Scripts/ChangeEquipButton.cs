﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeEquipButton : MonoBehaviour
{
    [SerializeField] private EquipType m_typeOfEquipment;
    [SerializeField] private bool m_goBack = false;

    public void ChangeIndex()
    {
        switch (m_typeOfEquipment)
        {
            case EquipType.Body:

                CameraController.Instance.SetAnimation("body");

                if (!m_goBack)
                {
                    SelectionManager.Instance.BodyIndex++;
                }
                else
                {
                    SelectionManager.Instance.BodyIndex--;
                }

                break;

            case EquipType.Engine:

                CameraController.Instance.SetAnimation("engine");

                if (!m_goBack)
                {
                    SelectionManager.Instance.EngineIndex++;
                }
                else
                {
                    SelectionManager.Instance.EngineIndex--;
                }
                break;

            case EquipType.Thrusters:

                CameraController.Instance.SetAnimation("thrusters");

                if (!m_goBack)
                {
                    SelectionManager.Instance.ThrustersIndex++;
                }
                else
                {
                    SelectionManager.Instance.ThrustersIndex--;
                }
                break;

            case EquipType.Wings:

                CameraController.Instance.SetAnimation("wings");

                if (!m_goBack)
                {
                    SelectionManager.Instance.WingsIndex++;
                }
                else
                {
                    SelectionManager.Instance.WingsIndex--;
                }
                break;
        }
    }
}
