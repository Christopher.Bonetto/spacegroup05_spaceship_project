﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EventID
{
    OnChangeEquipmentPreview,
    OnNewEquipmentCreated,
    OnChangeStats,
    OnNewCharacterCreated,
    OnChangeCharacterPreview,
    OnCharacterSelected
}
