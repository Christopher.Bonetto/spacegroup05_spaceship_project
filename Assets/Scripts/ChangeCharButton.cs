﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ChangeCharButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private float m_minAlphaValue = 0f;
    [SerializeField] private float m_timeToChangeAlpha = 0f;

    private int m_charIndex = 0;
    public int CharIndex
    {
        get
        {
            return m_charIndex;
        }
        set
        {
            m_charIndex = value;
        }
    }

    private Image m_myImage;



    private void Awake()
    {
        m_myImage = gameObject.GetComponent<Image>();
    }
    private void Start()
    {
        m_myImage.CrossFadeAlpha(m_minAlphaValue, 0, false);
    }



    public void SelectedCharacter()
    {
        SelectionManager.Instance.SelectedCharacter = SelectionManager.Instance.MyCollection.ListOfAllCharacter.Characters[CharIndex];
    }

    public void SetNewSprite(Sprite inSprite)
    {
        m_myImage.sprite = inSprite;
        m_myImage.preserveAspect = true;
    }


    public void OnPointerEnter(PointerEventData eventData)
    {
        SelectionManager.Instance.CharactersIndex = CharIndex;

        m_myImage.CrossFadeAlpha(1, m_timeToChangeAlpha, false);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        m_myImage.CrossFadeAlpha(m_minAlphaValue, m_timeToChangeAlpha, false);
    }
}
