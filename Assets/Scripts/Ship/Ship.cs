﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship : MonoBehaviour
{
    #region Wings

    private GameObject m_currentWings;
    public GameObject CurrentWings
    {
        get
        {
            return m_currentWings;
        }
        private set
        {
            if (m_currentWings != null)
            {
                m_currentWings.SetActive(false);
            }
            m_currentWings = value;
            m_currentWings.SetActive(true);
        }
    }

    private Equipment m_shipWings;
    public Equipment ShipWings
    {
        get
        {
            return m_shipWings;
        }
        private set
        {
            if(m_shipWings != null)
            {
                ChangeStats(m_shipWings.m_equipmentStats, false);
            }
            m_shipWings = value;

            ChangeStats(m_shipWings.m_equipmentStats, true);
        }
    }

    #endregion

    #region Engine

    private GameObject m_currentEngine;
    public GameObject CurrentEngine
    {
        get
        {
            return m_currentEngine;
        }
        private set
        {
            if (m_currentEngine != null)
            {
                m_currentEngine.SetActive(false);
            }
            m_currentEngine = value;
            m_currentEngine.SetActive(true);
        }
    }

    private Equipment m_shipEngine;
    public Equipment ShipEngine
    {
        get
        {
            return m_shipEngine;
        }
        private set
        {
            if (m_shipEngine != null)
            {
                ChangeStats(m_shipEngine.m_equipmentStats, false);
            }
            m_shipEngine = value;
            ChangeStats(m_shipEngine.m_equipmentStats, true);
        }
    }
    #endregion

    #region Thrusters

    private GameObject m_currentThrusters;
    public GameObject CurrentThrusters
    {
        get
        {
            return m_currentThrusters;
        }
        private set
        {
            if (m_currentThrusters != null)
            {
                m_currentThrusters.SetActive(false);
            }
            m_currentThrusters = value;
            m_currentThrusters.SetActive(true);
        }
    }

    private Equipment m_shipThrusters;
    public Equipment ShipThrusters
    {
        get
        {
            return m_shipThrusters;
        }
        private set
        {
            if (m_shipThrusters != null)
            {
                ChangeStats(m_shipThrusters.m_equipmentStats, false);
            }
            m_shipThrusters = value;
            ChangeStats(m_shipThrusters.m_equipmentStats, true);
        }
    }
    #endregion

    #region Body

    private Body m_body;
    public Body Body
    {
        get
        {
            return m_body;
        }
        private set
        {
            if (m_body != null)
            {
                m_body.gameObject.SetActive(false);
            }
            m_body = value;
            m_body.gameObject.SetActive(true);


            if (m_currentEngine != null)
            {
                m_body.AssignEquipment(m_currentEngine, m_body.EnginePosition);
            }
            if (m_currentThrusters != null)
            {
                m_body.AssignEquipment(m_currentThrusters, m_body.ThrustersPosition);
            }
            if (m_currentWings != null)
            {
                m_body.AssignEquipment(CurrentWings.transform.GetChild(0).gameObject, m_body.LeftWingPosition);
                m_body.AssignEquipment(CurrentWings.transform.GetChild(1).gameObject, m_body.RightWingPosition);
            }
        }
    }

    private Equipment m_shipBody;
    public Equipment ShipBody
    {
        get
        {
            return m_shipBody;
        }
        private set
        {
            if (m_shipBody != null)
            {
                ChangeStats(m_shipBody.m_equipmentStats, false);
            }
            m_shipBody = value;
            ChangeStats(m_shipBody.m_equipmentStats, true);
        }
    }
    #endregion

    #region Character in ship

    public Character m_charInShip;
    public Character CharInShip
    {
        get
        {
            return m_charInShip;
        }
        set
        {
            m_charInShip = value;

            CheckCharacterValue(m_charInShip);
        }
    }

    #endregion

    #region Ship stats

    public Stats m_shipStats;
    public Stats ShipStats
    {
        get
        {
            return m_shipStats;
        }
        set
        {
            m_shipStats.Acceleration += value.Acceleration;
            m_shipStats.Speed += value.Speed;
            m_shipStats.Manovrability += value.Manovrability;
            m_shipStats.Mass += value.Mass;

            EventManager.TriggerEvent<Stats>(EventID.OnChangeStats, ShipStats);
        }
    }

    

    #endregion

    #region Events

    private void OnEnable()
    {
        EventManager.SubscribeTo<Equipment, GameObject>(EventID.OnChangeEquipmentPreview, ChangeEquipment);

        EventManager.SubscribeTo<Character>(EventID.OnCharacterSelected, ChangeCharacterOnShip);
    }
    private void OnDisable()
    {
        EventManager.UnsubscribeFrom<Equipment, GameObject>(EventID.OnChangeEquipmentPreview, ChangeEquipment);

        EventManager.UnsubscribeFrom<Character>(EventID.OnCharacterSelected, ChangeCharacterOnShip);
    }

    #endregion

    #region Change piece of ship

    public void ChangeEquipment(Equipment inNewEquip, GameObject inNewObj)
    {
        EquipType type = inNewEquip.CurrentEquipType;

        switch (type)
        {
            case EquipType.Body:
                ShipBody = inNewEquip;
                Body = inNewObj.GetComponent<Body>();
                break;

            case EquipType.Engine:
                if (m_body != null)
                {
                    ShipEngine = inNewEquip;

                    CurrentEngine = inNewObj;

                    m_body.AssignEquipment(m_currentEngine, m_body.EnginePosition);
                }
                break;

            case EquipType.Thrusters:
                if (m_body != null)
                {
                    ShipThrusters = inNewEquip;
                    
                    CurrentThrusters = inNewObj;

                    m_body.AssignEquipment(m_currentThrusters, m_body.ThrustersPosition);
                }
                break;

            case EquipType.Wings:
                if (m_body != null)
                {
                    ShipWings = inNewEquip;
                    
                    CurrentWings = inNewObj;

                    m_body.AssignEquipment(CurrentWings.transform.GetChild(0).gameObject, m_body.LeftWingPosition);
                    m_body.AssignEquipment(CurrentWings.transform.GetChild(1).gameObject, m_body.RightWingPosition);
                }
                break;

                
        }
    }

    public void ChangeStats(Stats inStats, bool wantToAdd)
    {
        Stats tempStats = new Stats();

        if (wantToAdd)
        {
            tempStats.Acceleration += inStats.Acceleration;
            tempStats.Speed += inStats.Speed;
            tempStats.Manovrability += inStats.Manovrability;
            tempStats.Mass += inStats.Mass;
        }
        else
        {
            tempStats.Acceleration -= inStats.Acceleration;
            tempStats.Speed -= inStats.Speed;
            tempStats.Manovrability -= inStats.Manovrability;
            tempStats.Mass -= inStats.Mass;
        }

        ShipStats = tempStats;
    }

    #endregion

    public void ChangeCharacterOnShip(Character inChar)
    {
        CharInShip = inChar;
    }

    public void CheckCharacterValue(Character inChar)
    {
        if (inChar == null)
        {
            EventManager.TriggerEvent<Character>(EventID.OnCharacterSelected, null);
            return;
        }

        TakeBuffFromChar(inChar);
        TakeDebuffFromChar(inChar);
    }

    public void TakeBuffFromChar(Character inChar)
    {
        Stats tempValue = new Stats();

        switch (inChar.BuffStat.Type)
        {
            case TypeOfValue.Acceleration:
                tempValue.Acceleration = inChar.BuffStat.Value;
                break;
            case TypeOfValue.Speed:
                tempValue.Speed = inChar.BuffStat.Value;
                break;
            case TypeOfValue.Manovrability:
                tempValue.Manovrability = inChar.BuffStat.Value;
                break;
            case TypeOfValue.Mass:
                tempValue.Mass = inChar.BuffStat.Value;
                break;
        }

        ShipStats = tempValue;
    }

    public void TakeDebuffFromChar(Character inChar)
    {
        Stats tempValue = new Stats();

        switch (inChar.DebuffStat.Type)
        {
            case TypeOfValue.Acceleration:
                tempValue.Acceleration = inChar.DebuffStat.Value;
                break;
            case TypeOfValue.Speed:
                tempValue.Speed = inChar.DebuffStat.Value;
                break;
            case TypeOfValue.Manovrability:
                tempValue.Manovrability = inChar.DebuffStat.Value;
                break;
            case TypeOfValue.Mass:
                tempValue.Mass = inChar.DebuffStat.Value;
                break;
        }

        ShipStats = tempValue;
    }


}
