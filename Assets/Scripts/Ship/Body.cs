﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Body : MonoBehaviour
{
    [SerializeField] private Transform m_enginePosition;
    public Transform EnginePosition
    {
        get
        {
            return m_enginePosition;
        }
    }

    [SerializeField] private Transform m_thrustersPosition;
    public Transform ThrustersPosition
    {
        get
        {
            return m_thrustersPosition;
        }
    }

    [SerializeField] private Transform m_leftWingPosition;
    public Transform LeftWingPosition
    {
        get
        {
            return m_leftWingPosition;
        }
    }

    [SerializeField] private Transform m_rightWingPosition;
    public Transform RightWingPosition
    {
        get
        {
            return m_rightWingPosition;
        }
    }



    public void AssignEquipment(GameObject inObj, Transform inSpawn)
    {
        inObj.transform.SetPositionAndRotation(inSpawn.transform.position, inSpawn.transform.rotation);
    }


}
